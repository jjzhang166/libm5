#include "cFolder.h"
#include <windows.h>
#include <vector>
#include <fstream>
#include <io.h>

static bool isVer(std::ifstream& ifile, const std::string& filename) {
	// -1 不存在/权限
	if (::_access(filename.c_str(), 0)) return false;
	ifile.open(filename);
	if (!ifile.is_open()) return false;
	std::string line;
	while (getline(ifile, line)) {
		if (line.find("Version") != std::string::npos && line.find("3.0.169") != std::string::npos) {
			ifile.close();
			return true;
		}
	}
	ifile.close();
	return false;
}


static bool isVers(std::ifstream& ifile, const std::string& path) {
	const std::vector<std::string> files = { "res.ini", "update.ini" };
	for (const auto& file : files) {
		if (!isVer(ifile, path + file)) {
			return false;
		}
	}
	return true;
}


static bool is169(std::ifstream& ifile, std::string& pathori) {
	const std::vector<std::string> paths = { "", "MHXY-JD-3.0.169/", "梦幻西游/" };
	for (const auto& path : paths) {
		if (isVers(ifile, pathori + path)) {
			pathori += path;
			return true;
		}
	}
	return false;
}


std::string get169() {
	std::ifstream ifile;
	std::string path;
	if (true) {
		const std::vector<std::string> paths = {
			"Program Files (x86)/梦幻西游/",
			"Program Files (x86)/",
			"Program Files /梦幻西游/",
			"Program Files /",
			"Program Files/梦幻西游/",
			"Program Files/",
			"梦幻西游/",
			"",
		};
		
		for (char c = 'C'; c <= 'N'; ++c) {
			for (const auto& p : paths) {
				path = c;
				path = path + ":/" + p;
				if (isVers(ifile, path)) {
					return path;
				}
			}
		}

	}
	char arr[1024];
	memset(arr, 0, sizeof(arr));
	memcpy(arr, __argv[0], strlen(__argv[0]));
	int length = -1;
	while (arr[++length] != 0) {
		if (arr[length] == '\\') {
			arr[length] = '/';
		}
	}
	int pos = 0;
	while (length > 0) {
		if (arr[--length] != '/') continue;
		arr[length + 1] = 0;
		path = arr;
		if (is169(ifile, path)) return path;
	}
	MessageBox(nullptr, "找不到客户端\n版本：3.0.169", "3.0.169", MB_OK);
	path.clear();
	return path;
}